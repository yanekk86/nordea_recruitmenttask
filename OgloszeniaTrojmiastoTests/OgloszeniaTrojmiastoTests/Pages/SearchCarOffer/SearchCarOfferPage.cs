﻿using OgloszeniaTrojmiastoTests.Pages.SearchCarOffer.Popups.DistrictSelection;
using OgloszeniaTrojmiastoTests.Pages.SearchCarOffer.Sections;
using OpenQA.Selenium;
using OgloszeniaTrojmiastoTests.Base;

namespace OgloszeniaTrojmiastoTests.Pages
{
    public class SearchCarOfferPage : Page
    {
        public readonly SearchCriteriaSection SearchCriteriaSection;
        public readonly SearchResultsSection SearchResultsSection;
        public readonly DistrictSelectionPopup DistrictSelectionPopup;

        protected override string Url
        {
            get { return "https://ogloszenia.trojmiasto.pl/motoryzacja-sprzedam"; }
        }

        public SearchCarOfferPage(IWebDriver driver) : base(driver)
        {
            SearchCriteriaSection = new SearchCriteriaSection(driver);
            DistrictSelectionPopup = new DistrictSelectionPopup(driver);
            SearchResultsSection = new SearchResultsSection(driver);
        }
    }
}
