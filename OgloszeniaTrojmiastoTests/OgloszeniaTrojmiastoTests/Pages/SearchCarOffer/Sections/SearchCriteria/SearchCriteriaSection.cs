﻿using System;
using OgloszeniaTrojmiastoTests.Base;
using OgloszeniaTrojmiastoTests.Base.Fields;
using OgloszeniaTrojmiastoTests.DataModels;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace OgloszeniaTrojmiastoTests.Pages.SearchCarOffer.Sections
{
    public class SearchCriteriaSection : PageSection
    {
        [FindsBy(How = How.Id, Using = "field_marka_chosen")]
        private IWebElement _makeSelectorElement;

        private SingleSelectorField MakeSingleSelectorField
        {
            get { return new SingleSelectorField(_makeSelectorElement); }
        }

        [FindsBy(How = How.Id, Using = "field_model_chosen")]
        private IWebElement _modelSelectorElement;

        private SingleSelectorField ModelSingleSelectorField
        {
            get { return new SingleSelectorField(_modelSelectorElement); }
        }

        [FindsBy(How = How.XPath, Using = "//input[@name='rok_produkcji[]'][1]")]
        private IWebElement _productionYearFromField;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'sumo_paliwo[]')]")]
        private IWebElement _fuelTypeSelectorElement;

        private SingleSelectorField FuelTypeSingleSelectorField
        {
            get { return new SingleSelectorField(_fuelTypeSelectorElement); }
        }

        [FindsBy(How = How.CssSelector, Using = "label[for='field_czy_klimatyzacja_1']")]
        private IWebElement _airConditionCheckbox;

        [FindsBy(How = How.Id, Using = "location_map_selector_1")]
        private IWebElement _locationButton;

        [FindsBy(How = How.CssSelector, Using = "input[name=szukaj]")]
        private IWebElement _searchButton;

        public SearchCriteriaSection(IWebDriver driver) : base(driver)
        {

        }

        public void FillInSearchForm(CarOfferSearchCriteriaModel searchCriteria)
        {
            if (!string.IsNullOrEmpty(searchCriteria.Make))
            {
                MakeSingleSelectorField.SetValue(searchCriteria.Make);
                WaitForAjaxToLoad();
            }

            if (!string.IsNullOrEmpty(searchCriteria.Model))
            {
                ModelSingleSelectorField.SetValue(searchCriteria.Model);
                WaitForAjaxToLoad();
            }

            if (searchCriteria.YearFrom > 0)
                _productionYearFromField.SendKeys(searchCriteria.YearFrom.ToString());

            if(searchCriteria.HasAirCondition)
                _airConditionCheckbox.Click();
        }

        public void ClickLocationButton()
        {
            _locationButton.Click();
            WaitForAjaxToLoad();
        }

        public void ClickSearchButton()
        {
            _searchButton.Click();
            WaitForAjaxToLoad();
        }
    }
}
