﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using NUnit.Framework;
using OgloszeniaTrojmiastoTests.Base;
using OgloszeniaTrojmiastoTests.DataModels;
using OgloszeniaTrojmiastoTests.Pages.SearchCarOffer.Sections.SearchResults;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace OgloszeniaTrojmiastoTests.Pages.SearchCarOffer.Sections
{
    public class SearchResultsSection : PageSection
    {
        [FindsBy(How = How.CssSelector, Using = ".list-ogl .list-elem")]
        private IList<IWebElement> SearchResultListItems;

        public SearchResultsSection(IWebDriver driver) : base(driver)
        {
        }

        public void AreAllResultsMatchCriteria(CarOfferSearchCriteriaModel searchCriteria)
        {
            foreach (var searchResult in ExtractSearchResults())
            {
                if (searchCriteria.Make != null && searchCriteria.Model != null)
                {
                    var expectedMakeAndModel = string.Format("{0} {1}", searchCriteria.Make, searchCriteria.Model);
                    var actualMakeAndModel = searchResult.MakeAndModel;
                    StringAssert.StartsWith(expectedMakeAndModel, actualMakeAndModel,
                        "Make and model aren't matching expected criteria");
                }

                if (searchCriteria.YearFrom > int.MinValue)
                {
                    var expectedProductionYearFrom = searchCriteria.YearFrom;
                    var actualProductionYearFrom = searchResult.ProductionYear;
                    Assert.GreaterOrEqual(expectedProductionYearFrom, actualProductionYearFrom,
                        "Production year doesn't meet expected search criteria");
                }

                if (!string.IsNullOrEmpty(searchCriteria.City))
                {
                    var expectedCity = searchCriteria.City;
                    var actualCity = searchResult.City;
                    StringAssert.StartsWith(expectedCity, actualCity,
                        "City doesn't start with expecteded value");
                }
            }
        }

        public void AreSearchResultsVisible()
        {
            CollectionAssert.IsNotEmpty(SearchResultListItems);
        }

        private IEnumerable<SearchResultModel> ExtractSearchResults()
        {
            var productionYearExtractionRegex = new Regex(@"\d+");
            foreach (var searchResultListItem in SearchResultListItems)
            {
                var makeAndModel = searchResultListItem
                    .FindElement(By.CssSelector(".marka.model.wersja"))
                    .Text;
                var city = searchResultListItem
                    .FindElement(By.ClassName("place"))
                    .Text;

                var productionYearElement = searchResultListItem
                    .FindElement(By.ClassName("rok_produkcji"));
                var productionYearMatch = productionYearExtractionRegex.Match(productionYearElement.Text).Value;
                var productionYear = int.Parse(productionYearMatch);

                yield return new SearchResultModel
                {
                    MakeAndModel = makeAndModel,
                    City = city,
                    ProductionYear = productionYear
                };
            }
        }
    }
}
