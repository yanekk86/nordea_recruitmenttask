﻿namespace OgloszeniaTrojmiastoTests.Pages.SearchCarOffer.Sections.SearchResults
{
    public class SearchResultModel
    {
        public string MakeAndModel;
        public int ProductionYear;
        public string City;
    }
}
