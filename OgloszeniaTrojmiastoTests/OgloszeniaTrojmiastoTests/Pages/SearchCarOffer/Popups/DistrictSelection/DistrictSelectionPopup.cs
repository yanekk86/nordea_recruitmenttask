﻿using System;
using OgloszeniaTrojmiastoTests.Base;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Linq;

namespace OgloszeniaTrojmiastoTests.Pages.SearchCarOffer.Popups.DistrictSelection
{
    public class DistrictSelectionPopup : PageSection
    {
        [FindsBy(How = How.CssSelector, Using = "ul.select_city a")]
        private IList<IWebElement> CitySelectionLabels;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'location_select_box') and not(contains(@style, 'display: none'))]//span[@class='location_select_all']")]
        private IWebElement AllDistrictsLabel;

        [FindsBy(How = How.CssSelector, Using = ".acceptDistricts")]
        private IWebElement AcceptButton;

        public DistrictSelectionPopup(IWebDriver driver) : base(driver)
        {
        }

        public void SelectCity(string cityName)
        {
            var citySelectionLabel = CitySelectionLabels
                .SingleOrDefault(label => label.Text == cityName);
            if(citySelectionLabel == null)
                throw new Exception(string.Format("Cannot find city label to select: {0}", cityName));
            citySelectionLabel.Click();
        }

        public void SelectAllDistricts()
        {
            AllDistrictsLabel.Click();
        }

        public void ClickAcceptButton()
        {
            AcceptButton.Click();
            WaitForAjaxToLoad();
        }
    }
}
