﻿using NUnit.Framework;
using OgloszeniaTrojmiastoTests.Base;
using OgloszeniaTrojmiastoTests.DataModels;
using OgloszeniaTrojmiastoTests.Pages;

namespace OgloszeniaTrojmiastoTests
{
    [TestFixture]
    public class SearchCarOfferTests : TestSuite
    {
        [Test]
        public void SearchForAudiA8_TestResultsAreFound()
        {
            var searchCriteria = new CarOfferSearchCriteriaModel
            {
                Make = "Audi",
                Model = "A8",
                FuelType = "diesel",
                HasAirCondition = true,
                YearFrom = 2005,
                City = "Gdańsk"
            };

            var page = GoTo<SearchCarOfferPage>();
            page.SearchCriteriaSection.FillInSearchForm(searchCriteria);

            page.SearchCriteriaSection.ClickLocationButton();
            page.DistrictSelectionPopup.SelectCity(searchCriteria.City);
            page.DistrictSelectionPopup.SelectAllDistricts();
            page.DistrictSelectionPopup.ClickAcceptButton();

            page.SearchCriteriaSection.ClickSearchButton();

            page.SearchResultsSection.AreSearchResultsVisible();
            page.SearchResultsSection.AreAllResultsMatchCriteria(searchCriteria);
        }
    }
}
