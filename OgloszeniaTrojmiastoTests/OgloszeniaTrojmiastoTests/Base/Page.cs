﻿using OpenQA.Selenium;

namespace OgloszeniaTrojmiastoTests.Base
{
    public abstract class Page : PageObject
    {
        protected abstract string Url { get; }

        public Page(IWebDriver driver) : base(driver)
        {

        }

        internal void Navigate()
        {
            Driver.Navigate().GoToUrl(Url);
            WaitForAjaxToLoad();
        }
    }
}
