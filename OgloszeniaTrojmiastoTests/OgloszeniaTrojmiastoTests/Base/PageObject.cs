﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;

namespace OgloszeniaTrojmiastoTests.Base
{
    public abstract class PageObject
    {
        protected readonly IWebDriver Driver;

        public PageObject(IWebDriver driver)
        {
            Driver = driver;
        }

        protected void WaitForAjaxToLoad()
        {
            var webDriverWait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            webDriverWait.Until(d =>
            {
                var ajaxCallsCount = d.ExecuteJavaScript<Int64>("return jQuery.active");
                return ajaxCallsCount == 0;
            });
        }
    }
}
