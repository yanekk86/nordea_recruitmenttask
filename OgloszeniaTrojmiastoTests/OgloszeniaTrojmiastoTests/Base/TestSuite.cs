﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Linq;

namespace OgloszeniaTrojmiastoTests.Base
{
    public abstract class TestSuite
    {
        private IWebDriver _driver;

        [SetUp]
        public void InitializeDriver()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void CloseDriver()
        {
            while (_driver.WindowHandles.Count > 1)
            {
                _driver.SwitchTo().Window(_driver.WindowHandles.First());
                _driver.Close();
            }
            _driver.Quit();
        }

        public TPage GoTo<TPage>()
            where TPage : Page
        {
            var page = (TPage)Activator.CreateInstance(typeof(TPage), _driver);
            page.Navigate();
            return page;
        }
    }
}
