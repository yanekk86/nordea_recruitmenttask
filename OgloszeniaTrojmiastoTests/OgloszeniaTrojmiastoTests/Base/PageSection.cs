﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace OgloszeniaTrojmiastoTests.Base
{
    public abstract class PageSection : PageObject
    {
        public PageSection(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
        }
    }
}
