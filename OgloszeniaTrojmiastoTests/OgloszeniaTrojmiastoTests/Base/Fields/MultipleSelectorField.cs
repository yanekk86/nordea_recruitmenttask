﻿using System;
using System.Linq;
using OpenQA.Selenium;

namespace OgloszeniaTrojmiastoTests.Base.Fields
{
    public class MultipleSelectorField : Field<string[]>
    {
        public MultipleSelectorField(IWebElement fieldElement) : base(fieldElement)
        {
        }

        public override void SetValue(string[] values)
        {
            FieldElement.Click();
            foreach (var value in values)
            {
                var optionElement = FieldElement
                    .FindElements(By.CssSelector("ul.options li.opt"))
                    .SingleOrDefault(option => option.Text.StartsWith(value));
                if (optionElement == null)
                    throw new Exception(string.Format("Cannot find option with value: {0}", value));
                optionElement.Click();
            }
            FieldElement.Click();
        }
    }
}
