﻿using System;
using System.Linq;
using OpenQA.Selenium;

namespace OgloszeniaTrojmiastoTests.Base.Fields
{
    public class SingleSelectorField : Field<string>
    {
        public SingleSelectorField(IWebElement selectorElement) : base(selectorElement)
        {
        }

        public override void SetValue(string option)
        {
            while (!FieldElement.FindElement(By.CssSelector("ul.chosen-results")).Displayed)
            {
                FieldElement.Click();
            }
            var optionElement = FieldElement
                .FindElements(By.XPath(string.Format("//li[contains(text(), '{0}')]", option)))
                .FirstOrDefault();
            if(optionElement == null)
                throw new Exception(string.Format("Cannot find following option: {0}", option));
            optionElement.Click();
        }
    }
}
