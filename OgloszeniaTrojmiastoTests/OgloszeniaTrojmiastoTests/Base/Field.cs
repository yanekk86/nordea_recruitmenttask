﻿using OpenQA.Selenium;

namespace OgloszeniaTrojmiastoTests.Base
{
    public abstract class Field<TInputValue>
    {
        protected IWebElement FieldElement;
        public Field(IWebElement fieldElement)
        {
            FieldElement = fieldElement;
        }

        public abstract void SetValue(TInputValue value);
    }
}
