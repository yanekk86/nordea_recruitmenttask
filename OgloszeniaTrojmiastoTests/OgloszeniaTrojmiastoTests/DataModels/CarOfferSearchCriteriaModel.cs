﻿namespace OgloszeniaTrojmiastoTests.DataModels
{
    public class CarOfferSearchCriteriaModel
    {
        public string Make;
        public string Model;
        public bool HasAirCondition;
        public string FuelType;
        public int YearFrom;
        public string City;
    }
}
